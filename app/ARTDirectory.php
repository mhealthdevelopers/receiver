<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ARTDirectory extends Model
{
    protected $connection = 'art_directory';
    public $table = 'incoming_msg';
    public $timestamps = false;

    protected $fillable = [
        'source', 'destination', 'msg', 'receivedtime', 'reference', 'linkId'
    ];
}
